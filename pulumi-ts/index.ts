import * as aws from "@pulumi/aws";
import * as fs from "fs";
import * as os from "os";
import {InstanceTypes} from "@pulumi/aws/ec2";

const userData = `#!/bin/bash
sudo sed --in-place 's/^#\\?Port 22$/Port 22222/g' /etc/ssh/sshd_config
sudo service ssh restart
`;

const ami = aws.getAmi({
    mostRecent: true,
    nameRegex: "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
    owners: ["099720109477"]
});
export const amiName = ami.name;

const all_tcp = {cidrBlocks: ["0.0.0.0/0"], protocol: "tcp",};

const sg_www = new aws.ec2.SecurityGroup('www',
    {
        ingress: [
            {...all_tcp, fromPort: 80, toPort: 80},
            {cidrBlocks: ["0.0.0.0/0"], protocol: "tcp", fromPort: 443, toPort: 443},
        ]
    }
);

const sg_ssh = new aws.ec2.SecurityGroup('ssh',
    {
        ingress: [
            {cidrBlocks: ["0.0.0.0/0"], protocol: "tcp", fromPort: 22, toPort: 22},
            {cidrBlocks: ["0.0.0.0/0"], protocol: "tcp", fromPort: 22222, toPort: 22222},
        ]
    }
);

const key_pair = new aws.ec2.KeyPair('ikar',
    {
        keyName: 'ikar',
        publicKey: fs.readFileSync(os.homedir() + "/.ssh/ikar.pub").toString(),
    }
);

const ec2 = new aws.ec2.Instance('website', {
    ami: ami.id,
    instanceType: InstanceTypes.T3a_Micro,
    keyName: key_pair.keyName,
    securityGroups: [
        sg_ssh.name,
        sg_www.name,
        "default",
    ],
    userData: userData,
});

export const dns = ec2.publicDns;

const defaultVpc = aws.ec2.getVpc({default: true});

// create target group that defines health check
const tg = new aws.lb.TargetGroup("websiteTargetGroup", {
    port: 80,
    protocol: "HTTP",
    targetType: "instance",
    vpcId: defaultVpc.id,

    healthCheck: {
        path: "/",
        matcher: "200",
        healthyThreshold: 5,
        unhealthyThreshold: 2,
        // interval: 30
        // timeout: 5
    },
});

// add our EC2 to the target group
new aws.lb.TargetGroupAttachment("website-tg-attachment", {
    targetId: ec2.id,
    targetGroupArn: tg.arn,
});

// const subnet = vpc.subnets
const subnetIds = aws.ec2.getSubnetIds({vpcId: defaultVpc.id});


// LOAD BALANCER
// add load balancer
const lb = new aws.lb.LoadBalancer("website", {
    subnets: subnetIds.ids,
    securityGroups: [
        sg_www.id,
        aws.ec2.SecurityGroup.get("default", "sg-17fd1b7e").id
    ],
});

// forward balancer HTTP traffic to the target group that contains our EC2
new aws.lb.Listener("website-lb-listener", {
    port: 80,
    loadBalancerArn: lb.arn,
    defaultActions: [{
        type: "forward",
        targetGroupArn: tg.arn,
    }]
});

// Find an issued certificate (was created manually in AWS Console)
const cert = aws.acm.getCertificate({
    domain: "*.example.com",
    statuses: ["ISSUED"]
});

// forward balancer HTTPS traffic to the target group that contains our EC2
new aws.lb.Listener("website-lb-listener-https", {
    port: 443,
    protocol: "HTTPS",
    loadBalancerArn: lb.arn,
    certificateArn: cert.arn,
    defaultActions: [{
        type: "forward",
        targetGroupArn: tg.arn,
    }]
});


// ROUTE 53
new aws.route53.Record(
    "www",
    {
        type: "A",
        zoneId: aws.route53.getZone({name: "example.com."}).id,
        name: "www.example.com",
        aliases: [{
            name: lb.dnsName,
            evaluateTargetHealth: false,
            zoneId: lb.zoneId,
        }]
    }
);
