# infrastructure
# as code

<p style="text-align: right; font-size: small;">
by Ikar Pohorský
<br>
https://gitlab.com/ikar/infrastructure-as-code
</p>

Notes:
- intro
- tools available
- tools tested
- results

----
## infrastructure as code
- code as a documentation
- versioning
  <br><small>(history, blame me)</small>
- save your mouse
  <br><small>(code copy&paste vs click copy&paste)</small>
- easy deprovisioning
- pull requests

Notes:
- what was the instance type before we hotfix-enlarged it?
- faster changes
- review your infra

----
### configuration management
- Ansible
- Chef
- Puppet
- SaltStack

Notes:
- software configuration of existing hardware
- creating EC2 with ansible is doable, but not easy&reliable

----
### configuration orchestration
Designed to automate the deployment
  <br>of servers and other infrastructure

Notes:
- hardware configuration
- mention provisioning?

---
# orchestration tools

----
## cloud-specific tools
- Azure Resource Manager
- Google Cloud Deployment Manager
- AWS CloudFormation
- AWS Cloud Development Kit
  <br><small>(CloudFormation-based)</small>

----
## cloud-agnostic tools
- Terraform
- Pulumi

Notes:
- cloud-agnostic?


---
# Competition

----
## Competition

- create a machine for our website,
- that sits behind a load balancer
- update DNS record pointing to the load balancer

----
## Competition
<img src="competition.png">

----
## Competition
- find latest ubuntu AMI
- add 2 security groups (WWW, SSH)
- create EC2 machine + change SSH port
- upload a key-pair for SSH-ing to the machine
- create a load balancer poining to EC2 machine
- HTTPS listener using (already existing) certificate
- adjust Route 53 DNS record(s)

----
## Competition
<img src="competition-detail.png">

----
## Competitors
- Terraform
- AWS CDK in Python
- AWS CDK in Typescript
- Pulumi in Python
- Pulumi in Typescript

Notes:
- mention troposphere+stacker?

----
## Common
- support _diff_, _apply_ & _destroy_ ✓
- the deploy speed is about the same ✓
- very active development ✗

----
## Competitors
- Terraform
- ~AWS CDK in Python~
- AWS CDK in Typescript
- ~Pulumi in Python~
- Pulumi in Typescript

Notes:
- Pulumi@python terrible
- CDK@python just not pythonic

---
# Competition

---
## Terraform
- industry standard ✓
- documentation ✓
- OK-ish IDE support ✓
- HCL ?
- stacks are cumbersome ✗
- state needs to be handled manually ✗

Notes:
- since 2014
- no sources in IDE ✗
- HashiCorp Configuration Language vs. spreading resource among multi-lang
- the most mature of all - still it doesn't mean it's mature

----
## Terraform
<img src="tf-pycharm.png">

----
## Terraform
<img src="tf-plan.png">

Notes:
- you see the parameter names here

----
## Terraform
<img src="tf-state.png">

Notes:
- store in git?
- store on S3?
- locking


---
## Pulumi
- TypeScript ✓, (Python) and C#/.Net (beta)
- ($) web app: handles state, logs, ACL... ✓
- supports stacks ✓
- hides terraform complexity ✓
- handles secrets ✓
- price ✗
- doc ✗

Notes:
- since 2018
- terraform on steroids

----
## Pulumi
<img src="pulumi-typescript-pycharm.png">

----
## Pulumi
<img src="pulumi-diff.png">

----
## Pulumi
<img src="pulumi-app.png">

----
## Pulumi
<img src="pulumi-app-stack.png">

----
## Pulumi
<img src="pulumi-app-activity.png">

----
## Pulumi
<img src="pulumi-app-resources.png">

----
## Pulumi Pricing
- free for individual use
- $50 per month for teams <= 3 ppl
- $75 per user/month for teams > 3 ppl



---
## AWS CDK
- TypeScript ✓, (Python), Java, and C#/.Net
- supports stacks ✓
- cool Visual Studio Code plugin ✓
- AWS Console CloudFormation UI ✓
- AWS documentation ?
- builds on non-OS AWS CloudFormation ✗

Notes:
- since 2018
- stacks in different regions ✓
- some say CloudFormation is behind Terraform sometimes ([e.g. EC2 key-pair](https://github.com/aws/aws-cdk/issues/5252))

----
## AWS CDK
<img src="cdk-cloudformation.png">

----
## AWS CDK
<img src="cdk-typescript-pycharm.png">

----
## AWS CDK
<img src="cdk-diff.png">

----
## AWS CDK
<img src="cdk-vs-code-plugin.png">

Notes:
- Visual Studio Code plugin



---
# Winner(s)!

----
### Terraform
You want cloud-agnostic mature tool<br>with a large community.

----
### Pulumi
You want cloud-agnostic tool and <br>don't want to learn a new language.

<small>and</small>

You want a lot of features and <br>you're able to pay for it.
<br><small>(Or individual use.)</small>

----
### AWS CDK
You want to build mainly lambda-based project.
<br><small>(in AWS ofc.)</small>

You use Visual Studio Code.

Notes:
- compared to SLS -> IAM is resolved out of the box

---
# Questions?

<p style="text-align: right; font-size: small;">
by Ikar Pohorský
<br>
https://gitlab.com/ikar/infrastructure-as-code
</p>
