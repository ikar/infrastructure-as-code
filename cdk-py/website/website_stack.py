from aws_cdk import (
    core,
    aws_ec2,
    aws_elasticloadbalancingv2,
    aws_route53,
    aws_route53_targets,
)


class WebsiteStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.vpc = aws_ec2.Vpc.from_lookup(self, 'default_vpc', is_default=True)

        self.sg_www = aws_ec2.SecurityGroup(
            self,
            'www',
            vpc=self.vpc,
            description="Allow WWW from anywhere",
            security_group_name="WWW from anywhere"
        )
        self.sg_www.add_ingress_rule(aws_ec2.Peer.any_ipv4(), aws_ec2.Port.tcp(80))
        self.sg_www.add_ingress_rule(aws_ec2.Peer.any_ipv4(), aws_ec2.Port.tcp(443))

        self.sg_ssh = aws_ec2.SecurityGroup(
            self,
            'ssh',
            vpc=self.vpc,
            description="Allow SSH from anywhere",
            security_group_name="SSH from anywhere"
        )
        self.sg_ssh.add_ingress_rule(aws_ec2.Peer.any_ipv4(), aws_ec2.Port.tcp(22))
        self.sg_ssh.add_ingress_rule(aws_ec2.Peer.any_ipv4(), aws_ec2.Port.tcp(22222))

        ami = aws_ec2.LookupMachineImage(
            name="ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
            owners=["099720109477"],
        )

        self.change_ssh_port = aws_ec2.UserData.for_linux()  # adds bash shebang
        self.change_ssh_port.add_commands(
            "sudo sed --in-place 's/^#\\?Port 22$/Port 22222/g' /etc/ssh/sshd_config",
            "sudo service ssh restart",
        )

        ec2 = aws_ec2.Instance(
            self,
            'website',
            instance_type=aws_ec2.InstanceType('t3a.micro'),
            machine_image=ami,
            vpc=self.vpc,
            security_group=self.sg_ssh,
            key_name="ikar",
            user_data=self.change_ssh_port,
        )
        ec2.add_security_group(self.sg_www)

        core.CfnOutput(
            scope=self,
            id="PublicIp",
            value=ec2.instance_public_dns_name,
            description="public ip website",
            export_name="website-public-ip")

        tg = aws_elasticloadbalancingv2.ApplicationTargetGroup(
            self,
            'website-target-group',
            protocol=aws_elasticloadbalancingv2.ApplicationProtocol.HTTP,
            port=80,
            target_type=aws_elasticloadbalancingv2.TargetType.INSTANCE,

            vpc=self.vpc,

            health_check=aws_elasticloadbalancingv2.HealthCheck(
                healthy_http_codes='200',
            ),
        )

        # https://github.com/aws/aws-cdk/issues/5256
        tg.add_target(aws_elasticloadbalancingv2.InstanceTarget(ec2.instance_id, port=80))

        lb = aws_elasticloadbalancingv2.ApplicationLoadBalancer(
            self,
            'website-load-balancer',
            vpc=self.vpc,
            internet_facing=True,
            security_group=self.sg_www,
        )
        listener = lb.add_listener("website-listener", port=80)
        listener.add_target_groups("http", target_groups=[tg])

        # example.com. zone
        zone = aws_route53.HostedZone.from_lookup(
            self,
            'example.com.',
            domain_name='example.com.'
        )
        # route 53
        aws_route53.ARecord(
            self,
            'www',
            zone=zone,
            record_name='www.example.com',
            target=aws_route53.RecordTarget.from_alias(aws_route53_targets.LoadBalancerTarget(lb))
        )
