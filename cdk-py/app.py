#!/usr/bin/env python3

from aws_cdk import core

from website.website_stack import WebsiteStack


app = core.App()
WebsiteStack(app, "hello", env={
    'region': 'eu-central-1',
    'account': '123123123123',
})

app.synth()
