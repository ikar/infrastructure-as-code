import cdk = require('@aws-cdk/core');
import ec2 = require("@aws-cdk/aws-ec2");


export class CdkTsStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const vpc = ec2.Vpc.fromLookup(this, "VPC", {isDefault: true,});

        const sg_ssh = new ec2.SecurityGroup(this, "sg-ssh", {
            securityGroupName: "sg-ssh",
            vpc: vpc,
        });
        sg_ssh.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22));
        sg_ssh.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22222));

        const ami = new ec2.LookupMachineImage({
            name: "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
            owners: ["099720109477"],
        });

        const instance = new ec2.Instance(this, "website", {
            instanceType: new ec2.InstanceType('t3a.micro'),
            securityGroup: sg_ssh,
            machineImage: ami,
            vpc: vpc
        });

        const sg_www = new ec2.SecurityGroup(this, "sg-www", {
            securityGroupName: "sg-www",
            vpc: vpc,
        });
        sg_www.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80));
        sg_www.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443));

        // TODO incomplete...
    }
}

