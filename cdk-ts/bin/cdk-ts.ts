#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { CdkTsStack } from '../lib/cdk-ts-stack';

const app = new cdk.App();
new CdkTsStack(app, 'CdkTsStack', {
    env: {
        region: 'eu-central-1',
        account: '123123123123',
    }
});
