from pathlib import Path

import pulumi
import pulumi_aws as aws

user_data = """#!/bin/bash
sudo sed --in-place 's/^#\\?Port 22$/Port 22222/g' /etc/ssh/sshd_config
sudo service ssh restart
"""

if __name__ == '__main__':
    all_tcp = {'cidr_blocks': ["0.0.0.0/0"], 'protocol': 'TCP', }

    sg_www = aws.ec2.SecurityGroup(
        resource_name="public-www",
        ingress=[
            {**all_tcp, 'from_port': 443, 'to_port': 443, },
            {**all_tcp, 'from_port': 80, 'to_port': 80, },
        ]
    )

    sg_ssh = aws.ec2.SecurityGroup(
        resource_name="public-ssh",
        ingress=[
            {**all_tcp, 'from_port': 22, 'to_port': 22, },
            {**all_tcp, 'from_port': 22222, 'to_port': 22222, },
        ]
    )

    with open(f"{Path.home()}/.ssh/ikar.pub", 'r') as f:
        ikar_key_pair = aws.ec2.KeyPair(
            "ikar",
            public_key=f.read()
        )

    ec2 = aws.ec2.Instance(
        "website",
        security_groups=[
            sg_www.name,
            sg_ssh.name,
            "default",
        ],
        ami=aws.get_ami(
            most_recent=True,
            name_regex="ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
            owners=["099720109477"],  # Canonical
        ).image_id,
        instance_type="t3a.micro",
        key_name=ikar_key_pair.key_name,
        user_data=user_data,
    )

    pulumi.export("DNS", ec2.public_dns)

    vpc_default = aws.ec2.get_vpc(default=True)

    wtg = aws.applicationloadbalancing.TargetGroup(
        "website-target-group",
        protocol="HTTP",
        port=80,

        vpc_id=vpc_default.id,

        health_check={
            'path': '/',
            'matcher': '200',
            'healthyThreshold': 5,
            'unhealthyThreshold': 2,
            'interval': 30,  # sec
            'timeout': 5,  # sec
        }
    )

    aws.applicationloadbalancing.TargetGroupAttachment(
        "website-target-group-attachment",
        port=80,
        target_group_arn=wtg.arn,
        target_id=ec2.id,
    )

    subnets = aws.ec2.get_subnet_ids(vpc_id=vpc_default.id)

    lb = aws.applicationloadbalancing.LoadBalancer(
        "website-lb",
        security_groups=[
            sg_www,
            aws.ec2.SecurityGroup.get("default", "sg-123asdfg"),
        ],
        subnets=subnets.ids
    )
    aws.applicationloadbalancing.Listener(
        "website-lb-listener",
        port=80,
        protocol="HTTP",
        load_balancer_arn=lb.arn,
        default_actions=[{
            "type": "forward",
            "target_group_arn": wtg.arn,
        }]
    )

    ap_zone = aws.route53.get_zone("example.com.")
    aws.route53.Record(
        "www.example.com",
        type="A",
        zone_id=ap_zone.id,
        name="new.example.com",
        aliases=[{
            "name": lb.dns_name,
            "evaluateTargetHealth": False,
            "zoneId": lb.zone_id,
        }]
    )
