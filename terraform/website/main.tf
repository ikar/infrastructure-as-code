# EC2
## create website first
resource "aws_instance" "website" {
  instance_type = "t3a.micro"

  ami = var.ami_id
  key_name = var.key_name

  tags = {
    Name = "website6",
    Alarm = "on",
  }

  security_groups = var.website_security_groups

  provisioner "local-exec" {
    command = "echo ${self.public_dns} > website_dns.txt"
  }

  # TODO: use user-data?
  # user_data = "..."
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("~/.ssh/ikar")
    host = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo sed --in-place 's/^#\\?Port 22$/Port 22222/g' /etc/ssh/sshd_config",
      "sudo service ssh restart",
    ]
  }
}

# LOAD BALANCER

# Use load balancer (https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html)
# to provide HTTPS

# TARGET GROUP

# get my VPC
data "aws_vpc" "selected" {}

# create target group that defines health check
resource "aws_lb_target_group" "website_target_group" {
  name = "website-target-group"
  protocol = "HTTP"
  port = 80
  target_type = "instance"

  vpc_id = data.aws_vpc.selected.id

  health_check {
    path = "/"
    matcher = 200
    healthy_threshold = 5
    unhealthy_threshold = 2
    interval = 30 # seconds
    timeout = 5 # seconds
  }
}

# add our EC2 to the target group
resource "aws_lb_target_group_attachment" "website_target_group_attachment" {
  port = 80

  target_group_arn = aws_lb_target_group.website_target_group.arn
  target_id = aws_instance.website.id
}

# LOAD BALANCER
data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
}

# add load balancer
resource "aws_lb" "website_lb" {
  name = "website-lb"
  load_balancer_type = "application"

  subnets = data.aws_subnet_ids.selected.ids
  security_groups = var.balancer_security_groups
}

# forward balancer HTTP traffic to the target group that contains our EC2
resource "aws_lb_listener" "website_lb_listener_http" {
  port = 80
  protocol = "HTTP"

  load_balancer_arn = aws_lb.website_lb.arn

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.website_target_group.arn
  }
}

# Find an issued certificate (was created manually in AWS Console)
data "aws_acm_certificate" "example_cert" {
  domain = "*.example.com"
  statuses = ["ISSUED"]
}

# forward balancer HTTPS traffic to the target group that contains our EC2
resource "aws_lb_listener" "website_lb_listener_https" {
  port = 443
  protocol = "HTTPS"

  load_balancer_arn = aws_lb.website_lb.arn
  certificate_arn = data.aws_acm_certificate.example_cert.arn

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.website_target_group.arn
  }
}

# ROUTE 53

data "aws_route53_zone" "selected" {
  name = "example.com."
}

resource "aws_route53_record" "new" {
  type = "A"

  zone_id = data.aws_route53_zone.selected.id
  name = "www.${data.aws_route53_zone.selected.name}"

  alias {
    name = aws_lb.website_lb.dns_name
    zone_id = aws_lb.website_lb.zone_id
    evaluate_target_health = false
  }
}
