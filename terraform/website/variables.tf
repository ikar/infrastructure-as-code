variable "website_security_groups" {
  type = list(string)
  description = "list of the security groups to be used"
}

variable "balancer_security_groups" {
  type = list(string)
  description = "list of the security groups to be used"
}

variable "key_name" {
  type = string
  description = "the name of the key to be used for website creation"
}

variable "ami_id" {
  type = string
  description = "the ID of the AMI to be useed for the new machine"
}

