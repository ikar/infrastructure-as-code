module "common" {
  source = "./common/"
}

module "website" {
  source = "./website/"
  ami_id = module.common.ubuntu_bionic_lts.id
  key_name = module.common.ikar_key_pair.key_name

  website_security_groups = [
    module.common.public_ssh.name,
    module.common.public_www.name,
    module.common.default.name,
  ]
  balancer_security_groups = [
    module.common.public_www.id,
    module.common.default.id,
  ]
}
