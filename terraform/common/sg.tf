resource "aws_security_group" "public_ssh" {
  name = "public-ssh"

  ingress {
    protocol = "TCP"
    from_port = 22
    to_port = 22
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    protocol = "TCP"
    from_port = 22222
    to_port = 22222
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "public_www" {
  name = "public-www"

  ingress {
    protocol = "TCP"
    from_port = 80
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    protocol = "TCP"
    from_port = 443
    to_port = 443
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-security-groups.html#default-security-group
data "aws_security_group" "default" {
  name = "default"
}

output "public_www" {
  value = aws_security_group.public_www
}

output "public_ssh" {
  value = aws_security_group.public_ssh
}

output "default" {
  value = data.aws_security_group.default
}
