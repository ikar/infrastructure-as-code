# select most recent ubuntu 18.04
data "aws_ami" "ubuntu_lts" {
  most_recent = true

  filter {
    name = "name"
    values = [
      "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = [
      "hvm"]
  }

  owners = [
    # Canonical
    "099720109477"
  ]
}

output "ubuntu_bionic_lts" {
  value = data.aws_ami.ubuntu_lts
}
