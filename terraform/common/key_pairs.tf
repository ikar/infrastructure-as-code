resource "aws_key_pair" "ikar" {
  key_name = "ikar"
  public_key = file("~/.ssh/ikar.pub")
}

output "ikar_key_pair" {
  value = aws_key_pair.ikar
}

